package com.example.workreport

import androidx.lifecycle.ViewModel

class WorkListViewModel : ViewModel() {

    val jobs = mutableListOf<Work>()

    init {
        for (i in 0 until 100) {
            val work = Work()
            work.title = "Work placeholder!"
            work.isDone =  i%2 == 0
            jobs.add(work)
        }

    }
}